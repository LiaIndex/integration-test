/**
 * Padding outputs 2 characters allways
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */

 const pad = (hex) => {
     return (hex.length === 1 ? "0" + hex : hex);
 }

 module.exports = {
     /**
      * converts RGB to hex string
      * @param {number} red 0-255
      * @param {number} green 0-255
      * @param {number} blue 0-255
      * @returns {string} hex value
      */
     rgbToHex: (red, green, blue) =>{
         const redHex = red.toString(16);
         const greenHex = green.toString(16);
         const blueHex = blue.toString(16);
         return pad(redHex) + pad(greenHex) + pad(blueHex);
     },

     /**
      * converts hex to rgb string[]
      * @param {string} hexvalue ff00000
      * @returns {int[3]} RGB colors
      */
     hexToRGB: (hexvalue) => {
         const redPart = parseInt(hexvalue.substring(0,2),16);
         const greenPart = parseInt(hexvalue.substring(2,4),16);
         const bluePart = parseInt(hexvalue.substring(4,6),16);
         return [redPart,greenPart,bluePart];
     }


 }
