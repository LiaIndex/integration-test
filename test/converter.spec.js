const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () =>{
    describe("RGB to Hex conversion", () =>{
        it("converts the basic colors", () =>{
            const redHex = converter.rgbToHex(255,0,0); //ff0000
            const greenHex = converter.rgbToHex(0,255,0); //00ff00
            const blueHex = converter.rgbToHex(0,0,255); //0000ff

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        });
    });
    describe("Hex to RGB conversion", () => {
        it("converts from hex to rgb", ()=>{
            const redHex = "ff0000";
            const colors = converter.hexToRGB(redHex);
            const red = colors[0];
            const green = colors[1];
            const blue = colors[2];
            expect(red).to.equal(255);
            expect(green).to.equal(0);
            expect(blue).to.equal(0);

        })
    });
});